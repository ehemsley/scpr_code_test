@app = angular.module 'scpr_test', []

@app.controller 'main_controller', ($scope, $http) ->
  $scope.submitQuery = ->
    $http.get("http://www.scpr.org/api/v3/articles?query=#{encodeURIComponent($scope.text)}").success (data) ->
      $scope.articles = data.articles
      return
    return

  $scope.download = (url) ->
    $http.post('/download', {url: url} ).success (data) ->
      a = document.createElement('a')
      a.setAttribute('href', data.download_link)
      a.setAttribute('download', data.download_link)
      a.click()
      return
    return

  $scope.play = (audio) ->
    audio.isLoading = true
    $http.post('/download', {url: audio.url} ).success (data) ->
      audio.isLoading = false
      audio.isPlaying = true

      audio.audioElement = document.createElement('audio')
      audio.audioElement.setAttribute('src', data.download_link)
      audio.audioElement.play()

      audio.audioElement.addEventListener 'ended', ->
        audio.isPlaying = false
        $scope.$apply()
        return

      return
    return

  $scope.stop = (audio) ->
    audio.isLoading = false
    audio.isPlaying = false

    audio.audioElement.pause()
    return

  return

